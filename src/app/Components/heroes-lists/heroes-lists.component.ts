import { Component } from '@angular/core';
import { Heroes } from 'src/Interfaces/heroes';
import { HEROES } from 'src/Mock-data/mock-heroes';


@Component({
  selector: 'app-heroes-lists',
  templateUrl: './heroes-lists.component.html',
  styleUrls: ['./heroes-lists.component.css']
})
export class HeroesListsComponent {
heroes= HEROES;

selectedHero?: Heroes;
onSelect(hero: Heroes): void {
this.selectedHero = hero;
}
}
