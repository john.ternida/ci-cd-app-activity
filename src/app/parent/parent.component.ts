import { Component } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent {
  items = ['item1', 'item2', 'item3', 'item4'];
  
  addItem(item: string){
    this.items.push(item);
  }
}
